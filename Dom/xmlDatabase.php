<?php
require("DBConnection.php");

function parseToXML($htmlStr)
{
$xmlStr=str_replace('<','&lt;',$htmlStr);
$xmlStr=str_replace('>','&gt;',$xmlStr);
$xmlStr=str_replace('"','&quot;',$xmlStr);
$xmlStr=str_replace("'",'&#39;',$xmlStr);
$xmlStr=str_replace("&",'&amp;',$xmlStr);
return $xmlStr;
}

// Select all the rows in the markers table
$sql_markers = "Select propertyID, propertyType, houseAddress, houseCity, houseZipCode, houseLatitude, houseLongitude From Properties, House WHERE Properties.houseID = House.houseID";
$result_markers = mysqli_query($connection, $sql_markers);


header("Content-type: text/xml");

// Start XML file, echo parent node
echo '<markers>';

// Iterate through the rows, printing XML nodes for each
while ($row = mysqli_fetch_assoc($result_markers)){
  // Add to XML document node
  echo '<marker ';
  echo 'propertyID="' . parseToXML($row['propertyID']) . '" ';
  echo 'houseAddress="' . parseToXML($row['houseAddress']) . '" ';
  echo 'houseCity="' . parseToXML($row['houseCity']) . '" ';
  echo 'houseLatitude="' . parseToXML($row['houseLatitude']) . '" ';
  echo 'houseLongitude="' . parseToXML($row['houseLongitude']) . '" ';
  echo 'propertyType="' . parseToXML($row['propertyType']) . '" ';
  echo '/>';
}

// End XML file
echo '</markers>';

?>