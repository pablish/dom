<?php
session_start();
require ("databaseController.php");
require ("landLordDBAPI.php");
$control = new databaseController();
$control->buttonSelectedCheck();




if($_POST['remove'])
{
	deleteHouse($houseID);
    printLandlordDashboard();
    printProperties();
}
elseif($_POST['update'])
{
	updateHouseForm();
	printLandlordDashboard();
    printProperties();
}
elseif($_POST['updateHouse'])
{
	updateHouse();
    printLandlordDashboard();
    printProperties();
}
elseif($_POST['insertLandlord'])
{
	
	showLandlordForm();
	
}
elseif($_POST['submitLandlord'])
{
    createHouse($houseAddress, $houseCity, $houseZipCode, $houseState, $houseYearBuilt, $housePrice);
    printLandlordDashboard();
    printProperties();
}
else
{
	printLandlordDashboard();
    printProperties();
}



function printLandlordDashboard()
{

    print
    "
    <div class='container'>
      <div class='row'>
        <div class='tm-section-header section-margin-top'>
          <div class='col-lg-4 col-md-3 col-sm-3'><hr></div>  
          <div class='col-lg-4 col-md-6 col-sm-6'><h3 class='tm-section-title'>Landlord Dashboard</h3></div>
          <div class='col-lg-4 col-md-3 col-sm-3'><hr></div>  
        </div>
      </div>
     </div>
     <div class='container'>
        <div class=\"col-xs-12 col-sm-12 col-md-6 col-lg-6 col-xs-offset-0 col-sm-offset-0 col-md-offset-3 col-lg-offset-3 toppad\" >
          <div class=\"panel panel-info\">
            <div class=\"panel-heading\">
              <h3 class=\"panel-title\">Landlord Information</h3>
            </div>
            <div class=\"panel-body\">
              <div class=\"row\">
                
                <div class=\" col-md-9 col-lg-9 \"> 
                  <table class=\"table table-user-information\">
                    <tbody>
                      <tr>
                        <td>First Name:</td>
                        <td>".$_SESSION['User_LANDLORD_ID']['landlordFirstName']." </td>
                      </tr>
                      
                      <tr>
                        <td>Last Name:</td>
                        <td>".$_SESSION['User_LANDLORD_ID']['landlordLastName']." </td>
                      </tr>
                     
                     </tbody>
                  </table>
                 </div>
              </div>
            </div>
          </div>
        </div>
     </div>
         
    ";

}


function printProperties()
{
print
"

<!DOCTYPE html>  
<html lang = en> 

	<head> <!-- start of head --> 
	
		<meta charset = 'utf-8'>
		<meta name='viewport' content='width=device-width, initial-scale=1.0'> 
   		<meta name='description' content='View All Properties'>
   		<meta name='author' content='Paul Jonathan Fairbrook'>
    		<link rel='icon' href='../favicon.ico' type='image/x-icon'> 


	  <!-- jQuery (necessary for Bootstraps JavaScript plugins) -->   
	   <script src='https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js'></script>  

	   <!-- Include all compiled plugins (below), or include individual files as needed -->
	   <script src='js/bootstrap.min.js'></script>

 	   <!--  style sheet linked in here  
 	   	<link rel='stylesheet' href='./css/ePlayers.css' type='text/css' />
 	   	<link rel='stylesheet' href='./css/footer.css' type='text/css' /> 
 	   	
 	   	-->   
 		<link rel = 'stylesheet' href = 'https://maxcdn.bootstrapcdn.com/bootstrap/3.3.2/css/bootstrap-theme.min.css'>
 		<link rel='stylesheet' type='text/css' href='//cdn.datatables.net/1.10.11/css/jquery.dataTables.css'> 
    	<script src = 'https://maxcdn.bootstrapcdn.com/bootstrap/3.3.2/js/bootstrap.min.js'></script>
        <link rel = 'stylesheet' href = 'https://maxcdn.bootstrapcdn.com/bootstrap/3.3.2/css/bootstrap.min.css'>
    	<script type='text/javascript' charset='utf-8' src='//cdn.datatables.net/1.10.11/js/jquery.dataTables.js'></script>
    	<script src='./js/dataTables.js'></script>
    




    <title>Landlord Dashboard</title>
    </head>
    
    <div class = wholePage> 
    <body> 

   <div class = 'container-fluid'> 
	
        <h1 id = heading> All Properties Page </h1> 

		<div class = 'table'>   
		<table id='table_id' class='display'>  
    		<thead>
        		<tr>
            			<th>Address</th>
            			<th>City</th> 
            			<th>State</th>
            			<th>Zip Code</th>
            			<th>Year Built</th>  
            			<th>Price</th>
            			<th>BedRooms</th>
            			<th>Bathrooms</th>
            			<th>Sq Ft</th>
            			<th>Edit</th>
            			<th>Delete</th>   
        		</tr>  
   			 </thead>  
	 
   		        <tfoot> 
        		<tr>  
            			<th>Address</th>
            			<th>City</th> 
            			<th>State</th>
            			<th>Zip Code</th>
            			<th>Year Built</th> 
            			<th>Price</th>
            			<th>BedRooms</th>
            			<th>Bathrooms</th>
            			<th>Sq Ft</th>
            			<th>Edit</th>
            			<th>Delete</th> 
        		</tr>  
                        
                </tfoot>  
   			 
   		<tbody> 
   		";
            returnAllProperties();
        
        print
        "
    		</tbody>
		
		</table> 



   </body> 



   </div>   <!-- end of wholePage class -->
 
  
  	    <form method= 'post' action='".$_SERVER['PHP_SELF']."'>\n
                	   <input type='hidden' name= 'insert' value='$houseID'/>\n 
                	  <button type='submit' value='Insert Property' name='insertLandlord' class='btn btn-info'>Insert New Landlord</button>   \n
            </form>   \n
  
  

<!--include('Footer.php')-->
</html>
"; 

}

function showLandlordForm()
{
	print
	        "  
	        <h2> Insert a new Property </h2>
               <form method='post' enctype='multipart/form-data' class='form-horizontal'>
               <div class='form'>   \n
  			
  			   <input name='houseAddress' class='form-control' placeholder='Enter Address' id='houseAddress'>    \n
  			   <br>   \n
  			   <input name='houseCity' class='form-control' placeholder='Enter City' id='houseCity'>    \n
  			   <br>   \n
  			   <input name='houseState' class='form-control' placeholder='Enter State' id='houseState'>    \n
  			   <br>   \n
  			   <input name='houseZipCode' class='form-control' placeholder='Enter Zip Code' id='houseZipCode'>    \n
  			   <br>   \n
  			   <input name='houseYearBuilt' class='form-control' placeholder='Enter Year Built' id='houseYearBuilt'>    \n
  			   <br>   \n
  			   <input name='housePrice' class='form-control' placeholder='Enter Price' id='housePrice'>    \n
  			   <br>   \n
                <input name='hiBedRooms' class='form-control' placeholder='Enter # of Bedrooms' id='hiBedRooms'>    \n
  			   <br>   \n
  			   <input name='hiBathRooms' class='form-control' placeholder='Enter # of Bathrooms' id='hiBathRooms'>    \n
  			   <br>   \n
  			   <input name='hiSqFt' class='form-control' placeholder='Enter Sq Ft' id='hiSqFt'>    \n
  			   <br>   \n

  			

  	    <br> \n
  	    <input type='submit' value='Submit' name='submitLandlord'/>\n
  			<input type = 'submit' value = 'cancel' name = 'cancel'/> \n
  			
		   </div>   \n
		   </form>  \n";
		

}



function printAll()

{

    print "POST array: \n";

    print_r($_POST);

    print "<br /> SESSION array: \n";

    print_r($_SESSION);

    print "<br />\n";

}// end of printAll




?>