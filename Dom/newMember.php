<?php

//print_r($_POST);

require ("databaseController.php");


$control = new databaseController();
$control->buttonSelectedCheck();

session_start();
printHTML();

function printHTML()
{
    
    require "header.php";
    headerSignup();
    
    print
    "
    <!DOCTYPE html>
    <html lang = 'en'>
    
    <head>
    
    <meta charset = 'utf-8'>
    <meta name='viewport' content='width=device-width, initial-scale=1.0'> 
    <meta name='description' content='Forgotten password'>
    <meta name='author' content='Paul Jonathan Fairbrook'>
    <link rel='icon' href='../favicon.ico' type='image/x-icon'> 
    
    <link rel = 'stylesheet' href = './css/newMember.css' type = 'text/css' />
    <script src = './js/newMember.js'></script>
    
    <title> Member Signup </title>
    
    </head>
    
    
        <div class='container'>
			<div class='row'>
				<div class='tm-section-header section-margin-top'>
					<div class='col-lg-4 col-md-3 col-sm-3'><hr></div>	
					<div class='col-lg-4 col-md-6 col-sm-6'><h3 class='tm-section-title'>Create New User</h3></div>
					<div class='col-lg-4 col-md-3 col-sm-3'><hr></div>	
				</div>
			</div>
        </div>
    
    
    
    
    
<div class='container'>
         <div class=\"col-xs-12 col-sm-12 col-md-6 col-lg-7 col-xs-offset-0 col-sm-offset-0 col-md-offset-3 col-lg-offset-3 toppad\" >
          <div class=\"panel panel-info\">
            <div class=\"panel-heading\">
              <h3 class=\"panel-title\">Basic Information</h3>
              <p><span class=\"error\">* required field.</span></p>
            </div>
            <div class=\"panel-body\">
              <div class=\"row\">
                
                <div class=\" col-md-9 col-lg-9 \"> 
                  <table class=\"table table-booking-information\">
                  
                  <form method=\"POST\" >

                    <tbody>
                      <tr>
                        <td>Email*</td>
                        <td>
                            <input type=\"text\" name=\"memberEmail\"><br>
                        </td>
                      </tr>
                      
                      <tr>
                        <td>Password*</td>
                        <td>
                                <input type=\"password\" name=\"memberPassword\"><br>
                        </td>
                      </tr>
                      
                        
                      <tr>
                        
                      </tr>
                      
                      <tr>
                        <td>First Name*</td>
                        <td>
                            <input type=\"text\" name=\"memberFirstName\"><br>
                        </td>
                      </tr>
                      <tr>
                        <td>Last Name*</td>
                        <td>
                            <input type=\"text\" name=\"memberLastName\"><br>
                        </td>
                      </tr>
                      <tr>
                        <td>Zip Code</td>
                        <td>
                                <input type=\"text\" name=\"memberZipCode\"><br>
                        </td>
                      </tr>
                        
                      <tr>
                        <td>
                      <input type='submit' name='submitNewUserInfo' class='tm-yellow-btn'></input>
                        </td>
                      </tr>

                     </tbody>
                     
                     </form>
                  </table>
                 </div>
              </div>
            </div>
          </div>
        </div>
        
        


     </div>   
     
  
     </div>";


}
?>



