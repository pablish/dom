<?php
require ("databaseController.php");
require ("header.php");
//$db = dbConnect();

$control = new databaseController();
$control->buttonSelectedCheck();

session_start();
mainHeader();
printMemberDashboard(); 


function printMemberDashboard()
{

    print
    "
    <div class='container'>
      <div class='row'>
        <div class='tm-section-header section-margin-top'>
          <div class='col-lg-4 col-md-3 col-sm-3'><hr></div>  
          <div class='col-lg-4 col-md-6 col-sm-6'><h3 class='tm-section-title'>Member Dashboard</h3></div>
          <div class='col-lg-4 col-md-3 col-sm-3'><hr></div>  
        </div>
      </div>
     </div>
     <div class='container'>
        <div class=\"col-xs-12 col-sm-12 col-md-6 col-lg-6 col-xs-offset-0 col-sm-offset-0 col-md-offset-3 col-lg-offset-3 toppad\" >
          <div class=\"panel panel-info\">
            <div class=\"panel-heading\">
              <h3 class=\"panel-title\">Member Information</h3>
            </div>
            <div class=\"panel-body\">
              <div class=\"row\">
                
                <div class=\" col-md-9 col-lg-9 \"> 
                  <table class=\"table table-user-information\">
                    <tbody>
                      <tr>
                        <td>First Name:</td>
                        <td>".$_SESSION['User_Member_ID']['memberFirstName']." </td>
                      </tr>
                      
                      <tr>
                        <td>Last Name:</td>
                        <td>".$_SESSION['User_Member_ID']['memberLastName']." </td>
                      </tr>
                     
                     </tbody>
                  </table>
                 </div>
              </div>
            </div>
          </div>
        </div>
     </div>
         
    ";

}




?>