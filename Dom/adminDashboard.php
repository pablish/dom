<?php


printAdminDashboard();


function printAdminDashboard()
{

    
print
"
    
    <!DOCTYPE html>  
    <html lang = 'en'>  

	<head> <!-- start of head -->  
	
		<meta charset = 'utf-8'> 
		<meta name='viewport' content='width=device-width, initial-scale=1.0'> 
   		<meta name='description' content='DOM admin Dashboard'> 
   		<meta name='author' content='Paul Jonathan Fairbrook'> 
    		<link rel='icon' href='../favicon.ico' type='image/x-icon'> 


	   <!-- jQuery (necessary for Bootstraps JavaScript plugins) -->    
	        <script src='https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js'></script>    

	   <!-- Include all compiled plugins (below), or include individual files as needed --> 
	        <script src='js/bootstrap.min.js'></script>    

 	   <!--  style sheet linked in here  -->    
 		    <link rel = 'stylesheet' href = 'https://maxcdn.bootstrapcdn.com/bootstrap/3.3.2/css/bootstrap-theme.min.css'>   
    	 	<script src = 'https://maxcdn.bootstrapcdn.com/bootstrap/3.3.2/js/bootstrap.min.js'></script>   
    	 	<link rel = 'stylesheet' href = 'https://maxcdn.bootstrapcdn.com/bootstrap/3.3.2/css/bootstrap.min.css'>
    	 	<link rel = 'stylesheet' type='text/css' href='//cdn.datatables.net/1.10.11/css/jquery.dataTables.css'>
    




        <title>Admin Dashboard</title>
    </head> 
    
    

   
    <div class='container'>
      <div class='row'>
        <div class='tm-section-header section-margin-top'>
          <div class='col-lg-4 col-md-3 col-sm-3'><hr></div>  
          <div class='col-lg-4 col-md-6 col-sm-6'><h3 class='tm-section-title'>Admin Dashboard</h3></div>
          <div class='col-lg-4 col-md-3 col-sm-3'><hr></div>  
        </div>
      </div>
     </div>
     <div class='container'>
        <div class=\'col-xs-12 col-sm-12 col-md-6 col-lg-6 col-xs-offset-0 col-sm-offset-0 col-md-offset-3 col-lg-offset-3 toppad\' >
          <div class=\'panel panel-info\'>
            <div class=\'panel-heading\'>
              <h3 class=\'panel-title\'>Employee Information</h3>
            </div>
            <div class=\'panel-body\'>
              <div class=\'row\'>
                
                <div class=\' col-md-9 col-lg-9 \'> 
                  <table class=\'table table-user-information\">
                    <tbody>
                      <tr>
                        <td>First Name:</td>
                        <td>".$_SESSION['User_ADMIN_ID']['adminFirstName']." </td>
                      </tr>
                      
                      <tr>
                        <td>Last Name:</td>
                        <td>".$_SESSION['User_ADMIN_ID']['adminLastName']." </td>
                      </tr>
                     
                      <tr>
                        <td>Employee ID:</td>
                        <td>".$_SESSION['User_ADMIN_ID']['adminID']."</td>
                      </tr>
                     </tbody>
                  </table>
                 </div>
              </div>
            </div>
          </div>
        </div>
     </div>
    
    <div class='container'>
            <div class='col-xs-12 col-sm-12 col-md-6 col-lg-6 col-xs-offset-0 col-sm-offset-0 col-md-offset-3 col-lg-offset-3 toppad' >
              <div class='panel panel-info'>
                <div class='panel-heading'>
                  <h3 class='panel-title'>Admin Tools</h3>
                </div>
         <div class='panel-body'>
                  <div class='row'>
                    <div class='form-group'>
                    <div class='col-xs-12 col-sm-12 col-md-6 col-lg-6 col-xs-offset-0 col-sm-offset-0 col-md-offset-3 col-lg-offset-3 toppad '> 
                        <form method = 'POST'>
                          
                            <div class='form-group text-center'>
                                  <a href='./viewAllMembers.php' class='btn' style='background-color:#FCDD44;' role='button' >View All Members</a>                             
                            </div>
                            <div class='form-group text-center'>
                                  <a href='./viewAllLandlords.php' class='btn' style='background-color:#FCDD44;' role='button' >View All Landlords</a>                             
                            </div>
                            <div class='form-group text-center'>
                                  <a href='./viewAllProperties.php' class='btn' style='background-color:#FCDD44;' role='button' >View Properties</a>                             
                            </div>
                           
                            
                        </form>
                     </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
        </div>
      </div>
         
    ";

}


?>