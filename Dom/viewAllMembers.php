<?php  
require_once "adminDBAPI.php";

printHTML();




function printHTML()
{
print
"

<!DOCTYPE html>  
<html lang = en> 

	<head> <!-- start of head --> 
	
		<meta charset = 'utf-8'>
		<meta name='viewport' content='width=device-width, initial-scale=1.0'> 
   		<meta name='description' content='View All Members'>
   		<meta name='author' content='Paul Jonathan Fairbrook'>
    		<link rel='icon' href='../favicon.ico' type='image/x-icon'> 


	  <!-- jQuery (necessary for Bootstraps JavaScript plugins) -->   
	   <script src='https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js'></script>  

	   <!-- Include all compiled plugins (below), or include individual files as needed -->
	   <script src='js/bootstrap.min.js'></script>

 	   <!--  style sheet linked in here  
 	   	<link rel='stylesheet' href='./css/ePlayers.css' type='text/css' />
 	   	<link rel='stylesheet' href='./css/footer.css' type='text/css' /> 
 	   	
 	   	-->   
 		<link rel = 'stylesheet' href = 'https://maxcdn.bootstrapcdn.com/bootstrap/3.3.2/css/bootstrap-theme.min.css'>
 		<link rel='stylesheet' type='text/css' href='//cdn.datatables.net/1.10.11/css/jquery.dataTables.css'> 
    	<script src = 'https://maxcdn.bootstrapcdn.com/bootstrap/3.3.2/js/bootstrap.min.js'></script>
        <link rel = 'stylesheet' href = 'https://maxcdn.bootstrapcdn.com/bootstrap/3.3.2/css/bootstrap.min.css'>
    	<script type='text/javascript' charset='utf-8' src='//cdn.datatables.net/1.10.11/js/jquery.dataTables.js'></script>
    	<script src='./js/dataTables.js'></script>
    




    <title>All Members</title>
    </head>
    
    <div class = wholePage> 
    <body> 

   <div class = 'container-fluid'> 
	
        <h1 id = heading> All Member&#39;s Page </h1> 

		<div class = 'table'>   
		<table id='table_id' class='display'>  
    		<thead>
        		<tr>
            			<th>First Name</th> 
            			<th>Last Name</th>  
            			<th>Member Since</th>
            			<th>Last Logged on</th>
            			<th>Delete</th>   
        		</tr>  
   			 </thead>  
	 
   		        <tfoot> 
        		<tr>  
            			<th>First Name</th> 
            			<th>Last Name</th>  
            			<th>Member Since</th>
            			<th>Last Logged on</th>
            			<th>Delete</th>  
        		</tr>  
                        
                </tfoot>  
   			 
   		<tbody> ";
            returnAllMembers();
print 
"
    		</tbody>
		
		</table> 



   </body> 



   </div>   <!-- end of wholePage class -->
   
   
   
            <div class='form-group text-center'>
                <a href='./adminDashboard.php' class='btn' style='background-color:#FCDD44;' role='button' >Back to Dashboard</a>                             
            </div>


<!--include('Footer.php')-->
</html>
"; 

}







?>  