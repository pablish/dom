<?php
require("header.php");
require("gridAPI.php");

session_start();
$coord = $_POST['location'];
$newCoord = explode(',',$coord);
$locationLat = $newCoord[0];
$locationLong = $newCoord[1];
$locationCity = $newCoord[2];



mainHeader();
printMap($locationLat, $locationLong);
printGrid($locationLat, $locationLong, $locationCity);


function printMap($locationLat, $locationLong)
{
    print
    "
   <!DOCTYPE html >
  <head>
    <meta name='viewport' content='initial-scale=1.0, user-scalable=no' />
    <meta http-equiv='content-type' content='text/html; charset=UTF-8'/>
     <script 
  src='https://maps.google.com/maps?file=api&v=2&sensor=false&hl=ko'      
  type='text/javascript'></script> 
    
    <style>
      /* Always set the map height explicitly to define the size of the div
       * element that contains the map. */
      #map {
        height: 100%;
      }
      /* Optional: Makes the sample page fill the window. */
      html, body {
        height: 100%;
        width: 70%;
        margin: 0;
        padding: 0;
      }
    </style>
  </head>

  <body>
    <div id='map'></div>

    <script>
      var customLabel = {
        restaurant: {
          House: 'H'
        },
        Apartment: {
          label: 'A'
        }
      };

        function initMap() 
        {
        var lat =  $locationLat
        var lng = $locationLong
        var map = new google.maps.Map(document.getElementById('map'), {
          center: new google.maps.LatLng(lat, lng),
          zoom: 13
        });
        var infoWindow = new google.maps.InfoWindow;



 // Change this depending on the name of your PHP or XML file
          downloadUrl('https://pfairbro.create.stedwards.edu/Dom/xmlDatabase.php', function(data) {
            var xml = data.responseXML;
            var markers = xml.documentElement.getElementsByTagName('marker');
            Array.prototype.forEach.call(markers, function(markerElem) {
              var propertyID = markerElem.getAttribute('propertyID');
              var houseAddress = markerElem.getAttribute('houseAddress');
              var houseCity = markerElem.getAttribute('houseCity');
              var point = new google.maps.LatLng(
                  parseFloat(markerElem.getAttribute('houseLatitude')),
                  parseFloat(markerElem.getAttribute('houseLongitude')));
              var propertyType = markerElem.getAttribute('type');

              var infowincontent = document.createElement('div');
              var strong = document.createElement('strong');
              strong.textContent = houseAddress
              infowincontent.appendChild(strong);
              infowincontent.appendChild(document.createElement('br'));

              var text = document.createElement('text');
              text.textContent = houseCity
              infowincontent.appendChild(text);
              var icon = customLabel[propertyType] || {};
              var marker = new google.maps.Marker({
                map: map,
                position: point,
                label: icon.label
              });
              marker.addListener('click', function() {
                infoWindow.setContent(infowincontent);
                infoWindow.open(map, marker);
              });
            });
          });
        }



      function downloadUrl(url, callback) {
        var request = window.ActiveXObject ?
            new ActiveXObject('Microsoft.XMLHTTP') :
            new XMLHttpRequest;

        request.onreadystatechange = function() {
          if (request.readyState == 4) {
            request.onreadystatechange = doNothing;
            callback(request, request.status);
          }
        };

        request.open('GET', url, true);
        request.send(null);
      }

      function doNothing() {}
    </script>
    <script async defer
    src='https://maps.googleapis.com/maps/api/js?key=AIzaSyDIBxiM2y4cuDHxcS7L-iInaXCX_-7HTik&language=en&callback=initMap'>
    </script>
   
  </body>
</html>
    ";
}



function printGrid( $locationLat,$locationLong, $locationCity)
{
   
    print
    "
    <!DOCTYPE html >
      <head>
        <meta name='viewport' content='initial-scale=1.0, user-scalable=no' />
        <meta http-equiv='content-type' content='text/html; charset=UTF-8'/>
    
    	   <!-- jQuery (necessary for Bootstraps JavaScript plugins) -->
    	   <script src='https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js'></script> 
    
    	   <!-- Include all compiled plugins (below), or include individual files as needed -->
    	   <script src='js/bootstrap.min.js'></script> 
            
         	<link rel = 'stylesheet' href = 'https://maxcdn.bootstrapcdn.com/bootstrap/3.3.2/css/bootstrap-theme.min.css'>  
     	    <link rel='stylesheet' type='text/css' href='//cdn.datatables.net/1.10.11/css/jquery.dataTables.css'>  
        	<script src = 'https://maxcdn.bootstrapcdn.com/bootstrap/3.3.2/js/bootstrap.min.js'></script>  
        	<link rel = 'stylesheet' href = 'https://maxcdn.bootstrapcdn.com/bootstrap/3.3.2/css/bootstrap.min.css'>
        	
        </head>
  

  
  
  </html>
  ";
  
    print
    "
        <body>
            <div class='row'> 
		        
		        
		            
    			
    ";
        gridView($locationLat, $locationLong, $locationCity);
    print
    "
                    
                 
                   
                
        </body>
    
    
    
    ";
    
}





function printAll()

{

    print "POST array: \n";

    print_r($_POST);

    print "<br /> SESSION array: \n";

    print_r($_SESSION);

    print "<br />\n";

}// end of printAll



?>