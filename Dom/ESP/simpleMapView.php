<?php
print
"
<!DOCTYPE html >
  <head>
    <meta name='viewport' content='initial-scale=1.0, user-scalable=no' />
    <meta http-equiv='content-type' content='text/html; charset=UTF-8'/>
    <title>Using MySQL and PHP with Google Maps</title>
    <style>
      /* Always set the map height explicitly to define the size of the div
       * element that contains the map. */
      #map {
        height: 100%;
      }
      /* Optional: Makes the sample page fill the window. */
      html, body {
        height: 100%;
        margin: 0;
        padding: 0;
      }
    </style>
  </head>

  <body>
    <div id='map'></div>

    <script>
      var customLabel = {
        house: {
          label: 'H'
        }
      };
          var map, infoWindow;
        function initMap() {
        map = new google.maps.Map(document.getElementById('map'), {
          center: {lat: 31.9686, lng: -99.9018},
          zoom: 14,
          gestureHandling: 'none',
          zoomControl: false
        });
        var infoWindow = new google.maps.InfoWindow;
        
        
        
        
        // Try HTML5 geolocation.
        if (navigator.geolocation) {
          navigator.geolocation.getCurrentPosition(function(position) {
            var pos = {
              lat: position.coords.latitude,
              lng: position.coords.longitude
            };

            infoWindow.setPosition(pos);
            infoWindow.setContent('Location found.');
            infoWindow.open(map);
            map.setCenter(pos);
          }, function() {
            handleLocationError(true, infoWindow, map.getCenter());
          });
        } else {
          // Browser doesn't support Geolocation
          handleLocationError(false, infoWindow, map.getCenter());
        }
      }

      function handleLocationError(browserHasGeolocation, infoWindow, pos) {
        infoWindow.setPosition(pos);
        infoWindow.setContent(browserHasGeolocation ?
                              'Error: The Geolocation service failed.' :
                              'Error: Your browser doesn\'t support geolocation.');
        infoWindow.open(map);
      }
        
        
        
        
        
        
        

          // Change this depending on the name of your PHP or XML file
          downloadUrl('https://pfairbro.create.stedwards.edu/Dom/xmlDatabase.php', function(data) {
            var xml = data.responseXML;
            var markers = xml.documentElement.getElementsByTagName('marker');
            Array.prototype.forEach.call(markers, function(markerElem) {
              var propertyID = markerElem.getAttribute('propertyID');
              var houseAddress = markerElem.getAttribute('houseAddress');
              var houseCity = markerElem.getAttribute('houseCity');
              var point = new google.maps.LatLng(
                  parseFloat(markerElem.getAttribute('houseLatitude')),
                  parseFloat(markerElem.getAttribute('houseLongitude')));
              var propertyType = markerElem.getAttribute('type');

              var infowincontent = document.createElement('div');
              var strong = document.createElement('strong');
              strong.textContent = name
              infowincontent.appendChild(strong);
              infowincontent.appendChild(document.createElement('br'));

              var text = document.createElement('text');
              text.textContent = houseAddress
              infowincontent.appendChild(text);
              var icon = customLabel[propertyType] || {};
              var marker = new google.maps.Marker({
                map: map,
                position: point,
                label: icon.label
              });
              marker.addListener('click', function() {
                infoWindow.setContent(infowincontent);
                //infoWindow.open(map, marker);
              });
            });
          });



      function downloadUrl(url, callback) {
        var request = window.ActiveXObject ?
            new ActiveXObject('Microsoft.XMLHTTP') :
            new XMLHttpRequest;

        request.onreadystatechange = function() {
          if (request.readyState == 4) {
            request.onreadystatechange = doNothing;
            callback(request, request.status);
          }
        };

        request.open('GET', url, true);
        request.send(null);
      }

      function doNothing() {}
    </script>
    <script async defer
    src='https://maps.googleapis.com/maps/api/js?key=AIzaSyDIBxiM2y4cuDHxcS7L-iInaXCX_-7HTik&callback=initMap'>
    </script>
  </body>
</html>
";
?>